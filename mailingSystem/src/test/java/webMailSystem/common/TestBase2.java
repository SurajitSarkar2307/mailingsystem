package webMailSystem.common;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.UnhandledAlertException;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.google.common.base.Throwables;

import webMailSystem.ui.ui2.pages.HeaderPage;
import webMailSystem.ui.ui2.pages.LoginPage;
import webMailSystem.ui.ui2.pages.WelcomePage;
import webMailSystem.utilities.MailContentReader;
import webMailSystem.utilities.MyExcelReader;
import webMailSystem.utilities.Utils;

public class TestBase2{
	protected static ExtentHtmlReporter htmlReporter;
	protected static ExtentReports extent;
	
	public ExtentTest parentTest;
	public ExtentTest childTest;
	protected LoginPage loginPage;
	protected WelcomePage welcomePage;
	public boolean isLoggedIn = true;
	public String emailContentFileName;
	public String dataFileName;
	public String emailContent;
	public String resourcePath = ".//resources";
	public static String executionReportFile;
	public static String xmlReportFile;
	public static Properties prop;
	public static Properties logProp;
	public LinkedList<String> subjectList;
	public static HashMap<Integer, LinkedList<Object>> subjectCount;
	public int dataSize;
	public String userName=null,password=null;
	public static void startReport() throws Exception{
		 executionReportFile=Utils.createHTMLReport(prop);
		 htmlReporter = new ExtentHtmlReporter(executionReportFile);
		 extent = new ExtentReports ();
		 extent.attachReporter(htmlReporter);
		 extent.setSystemInfo("Company Name", "Science Repo");	 
		 htmlReporter.config().setDocumentTitle("ScienceRepo-AutomationRun");
		 htmlReporter.config().setReportName("ScienceRepo-AutomationRun");
		 htmlReporter.config().setTestViewChartLocation(ChartLocation.TOP);
		 htmlReporter.config().setTheme(Theme.DARK);
		 }
	public static void loadProperty() throws IOException
	{
		prop = new Properties();
		logProp = new Properties();
			InputStream input1 ,input2= null;
			input1 = new FileInputStream(Utils.getCurrentWorkingDirectory()+"\\resources\\ui2\\config.properties");
			input2 = new FileInputStream(Utils.getCurrentWorkingDirectory()+"\\resources\\ui2\\log.properties");
			prop.load(input1);
			logProp.load(input2);
		
	}

	
	@BeforeSuite(alwaysRun=true)
	public static synchronized void beforeSuite() throws Exception
	{		
		loadProperty();
		xmlReportFile=Utils.createXMLReport(prop);
		Utils.copyFile(Utils.getCurrentWorkingDirectory()+"\\test-output\\testng-results.xml", xmlReportFile);
		startReport();
		subjectCount = new HashMap<Integer, LinkedList<Object>>();
		
	}
	
	@BeforeTest(alwaysRun=true)
	public synchronized void initBrowser(ITestContext testContext) throws Exception
	{
		parentTest=extent.createTest(testContext.getName());
		parentTest.log(Status.INFO, String.format("Execution reports willl be found at '%s'", executionReportFile));
		
		subjectList = MyExcelReader.readSubjects(parentTest,new File(resourcePath+"//SubjectList.xlsx"));
		HashMap<String, LinkedList<String>> loginData = MyExcelReader.readSetupData(parentTest,new File(resourcePath+"//ui2//setupFile.xlsx"));
		if(loginData.containsKey(testContext.getName()))
		{
			LinkedList<String> credentials = loginData.get(testContext.getName());
			if(credentials.get(0).equals("")||credentials.get(1).equals("")||credentials.get(2).equals("")||credentials.get(3).equals(""))
				{
					parentTest.log(Status.FATAL,String.format("Entry for '%s' test does not contains all the necessary information in the file.", testContext.getName()));
					Assert.fail();
				}
				else{
				userName=credentials.get(0);
				password=credentials.get(1);
				emailContentFileName=credentials.get(2);
				dataFileName=credentials.get(3);
				}
		}else{
			parentTest.log(Status.FATAL, String.format("Entry for '%s' test is not present in the file.",testContext.getName()));
			Assert.fail();
		}
		try{
		DriverFactory.getInstance().getDriver().manage().window().maximize();
		DriverFactory.getInstance().getDriver().manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		loginPage = navigateToUrl(prop.getProperty("Url"));
		welcomePage = loginPage.loginToWebmail(parentTest,userName, password);
		emailContent=MailContentReader.readMailContent(new File(resourcePath+"//ui2//"+emailContentFileName));
		emailContent=emailContent.replaceAll("[^\\x00-\\x7F]", " ");
	}catch(Exception e)
	{
		parentTest.log(Status.ERROR, "Unable to Initialize browser or Perform successful login");
		parentTest.log(Status.ERROR,Throwables.getStackTraceAsString(e));
		parentTest.addScreenCaptureFromPath(Utils.takeScreenShot(parentTest,testContext.getName()));
		throw e;
	}
	}
	
	@AfterTest(alwaysRun=true)
	public synchronized void cleanUp()
	{
		parentTest.log(Status.INFO, "Exiting..");
		DriverFactory.getInstance().removeDriver();
		
	}
	@AfterSuite(alwaysRun=true)
	public synchronized void afterSuite() throws Exception
	{
		printSubjectStats();
		Utils.setProperty(parentTest,prop,"config.properties", "LastExecutedTime", new SimpleDateFormat("hh_mm_ss_SSS").format(new Date()));	
		extent.flush();
			
	}
	public LoginPage navigateToUrl(String url) throws Exception
	{
		DriverFactory.getInstance().getDriver().get(url);
		return new LoginPage(parentTest);
	}
	
	@AfterMethod
	public void afterMethod(ITestResult testResult) throws IOException, Exception
	{
		if(testResult.getStatus()==ITestResult.FAILURE)
		{
			try{
			String testNameMethodName=testResult.getTestContext().getName()+"_"+testResult.getName().toString().trim();
			 	try {
			 		childTest.addScreenCaptureFromPath(Utils.takeScreenShot(childTest,testNameMethodName));
			 		try{
			 			//DriverFactory.getInstance().getDriver().switchTo().defaultContent();
				 		
			 			loginPage =new HeaderPage(childTest).clickOnLogout(childTest);
			 		isLoggedIn=false;
			 		}catch(Exception e)
			 		{
			 			isLoggedIn=false;
			 			childTest.addScreenCaptureFromPath(Utils.takeScreenShot(childTest,testNameMethodName));
			 			childTest.log(Status.FAIL, "Unable to logout.. "+Throwables.getStackTraceAsString(e));
			 		}
			} catch (UnhandledAlertException e) {
				Alert alert= DriverFactory.getInstance().getDriver().switchTo().alert();
				String title = alert.getText();
				alert.accept();
				childTest.addScreenCaptureFromPath(Utils.takeScreenShot(childTest,testNameMethodName));
				childTest.log(Status.FAIL, "Unhanded Alert was displayed - "+title);
				try{
		 			//DriverFactory.getInstance().getDriver().switchTo().defaultContent();
			 		
		 			loginPage =new HeaderPage(childTest).clickOnLogout(childTest);
		 		isLoggedIn=false;
		 		}catch(Exception e1)
		 		{
		 			isLoggedIn=false;
		 			childTest.addScreenCaptureFromPath(Utils.takeScreenShot(childTest,testNameMethodName));
		 			childTest.log(Status.FAIL, "Unable to logout.. "+Throwables.getStackTraceAsString(e1));
		 		}
				}
			 	catch (Exception e) {
			 		isLoggedIn=false;
				childTest.log(Status.FAIL, "Some Error occured for the test - "+Throwables.getRootCause(e));
				}
			}catch(Exception e)
			{
				isLoggedIn=false;
				childTest.log(Status.FAIL, "Some Error occured for the test - "+Throwables.getRootCause(e));
			}
		}else if(testResult.getStatus()==ITestResult.SKIP)
		{
			isLoggedIn=false;
			childTest.log(Status.SKIP, "Test case was skipped...");
		}
	}
	/**
	 * This method will update the subject line count used in the execution.
	 * @param index
	 * @param subject
	 */
	public static HashMap<Integer, LinkedList<Object>> updateSubjectCount(int index,String subject)
	{
		String subjectLine;
		int count;
		LinkedList<Object> stats = new LinkedList<Object>();
		if(subjectCount.containsKey(index))
		{
			subjectLine = String.valueOf(subjectCount.get(index).get(0));
			count = Integer.parseInt(String.valueOf(subjectCount.get(index).get(1)));
			count=count+1;	
		}else{
			count=1;
			subjectLine=subject;
		}
		subjectLine = subjectLine.replaceAll("[^\\x00-\\x7F]", "");
		stats.add(subjectLine);
		stats.add(count);
		subjectCount.put(index, stats);		
		return subjectCount;
	}
	public void printSubjectStats()
	{
		extent.setSystemInfo("Subject Stats", "-");
		for(Map.Entry<Integer, LinkedList<Object>> m:subjectCount.entrySet()){  
			   extent.setSystemInfo(String.valueOf(m.getKey()), String.valueOf(m.getValue().get(0)) +"  -  " +String.valueOf(m.getValue().get(1)));
			  }  
	}

}
