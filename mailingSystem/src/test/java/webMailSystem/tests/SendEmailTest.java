package webMailSystem.tests;

import java.io.File;
import java.lang.reflect.Method;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.UnhandledAlertException;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import com.google.common.base.Throwables;
import junit.framework.Assert;
import webMailSystem.common.DriverFactory;
import webMailSystem.common.TestBase;
import webMailSystem.ui.u1.pages.ComposeMailPage;
import webMailSystem.utilities.MyExcelReader;
import webMailSystem.utilities.Utils;

public class SendEmailTest extends TestBase {
	ComposeMailPage composeMailPage;
	@Test(dataProvider = "emailData")
	public synchronized void SendHTMLMail(String emailID, String name,String rowNumber,Method method,ITestContext testContext) throws Exception {
		try{
			
		childTest =parentTest.createNode(method.getName()+"_"+name);
		Utils.setProperty(childTest,logProp,"log.properties", testContext.getName()+"_LastRowExecutedData", String.valueOf(rowNumber)+" | "+emailID+" | "+name);	
		if(!isLoggedIn)
		{
			DriverFactory.getInstance().removeDriver();
			DriverFactory.getInstance().getDriver().manage().window().maximize();
			DriverFactory.getInstance().getDriver().manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
			loginPage = navigateToUrl(prop.getProperty("Url"));
			welcomePage = loginPage.loginToWebmail(childTest,userName, password);
			isLoggedIn=true;
		}
		composeMailPage = welcomePage.clickOnComposeMailBtn(childTest);
		Random random = new Random();
		int index =random.nextInt((subjectList.size()-0)+0)+0;
		String subject=subjectList.get(index);
		subject = subject.replaceAll("[^\\x00-\\x7F]", "");
		subjectCount=updateSubjectCount(index, subject);
		welcomePage = composeMailPage.composeHTMLMail(childTest,emailID, subject, emailContent.replace("{{Dynamic Param Name}}", name));
		Assert.assertTrue("Mail was not sent.. ", welcomePage!=null);
		}catch(UnhandledAlertException e)
		{
			throw e;
		}
		catch(Exception e)
		{
			childTest.log(Status.FAIL, "Send Mail Failed.. "+Throwables.getRootCause(e));
			throw e;
		}

	}
	@DataProvider(name="emailData")
	public String[][] emailData(ITestContext testcontext) throws Exception
	{
		String[][] data=null;
		File filePath = new File(resourcePath+"//ui1//"+dataFileName);
		String propName = testcontext.getName()+"_Start";
		if(prop.containsKey(propName))
		{
			int start = Integer.parseInt(prop.getProperty(propName));

		dataSize = Integer.parseInt(prop.getProperty("DataSize"));
		data= MyExcelReader.readTestData(parentTest,filePath,start,dataSize);
		if(data!=null)
		{
			int lastRow =MyExcelReader.getLastRowNumberToBeExecuted(parentTest, filePath, start, dataSize);
			if(lastRow>0)
			{
				Utils.setProperty(parentTest,prop,"config.properties", testcontext.getName()+"_Start", String.valueOf(lastRow+1));
				parentTest.log(Status.INFO, "Updated the next Start Row in the config file for the property - "+testcontext.getName()+"_Start to "+(lastRow+1));
			}else{
				parentTest.log(Status.WARNING, "Unable to update the next Start Row in the config file for the property - "+testcontext.getName()+"_Start");
			}
		}
		
		return data;	
		}else{
			parentTest.log(Status.FAIL, "Config file does not contains the entry for the property name " +propName);
			return data;
		}
		
	}

}
