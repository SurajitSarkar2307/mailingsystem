package webMailSystem.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.LinkedList;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.google.common.base.Throwables;
public class MyExcelReader {
	public static HashMap<String, LinkedList<String>> readSetupData(ExtentTest extentTest,File filePath) throws Exception{

	    HashMap<String, LinkedList<String>> setupData = new HashMap<String, LinkedList<String>>();
		try{
	    FileInputStream inputStream = new FileInputStream(filePath);

	    Workbook setupWorkbook = null;

	    setupWorkbook = new XSSFWorkbook(inputStream);
	    

	    Sheet setupSheet = setupWorkbook.getSheetAt(0);

	    int rowCount = setupSheet.getPhysicalNumberOfRows()-1;
	    int colCount=setupSheet.getRow(0).getLastCellNum();
	    for (int i = 0; i < rowCount; i++) {

	        Row row = setupSheet.getRow(i+1);
	        LinkedList<String> data = new LinkedList<String>();
	        for(int col=1;col<colCount;col++)
	        {
	            data.add(row.getCell(col).getStringCellValue());
	        }
	            setupData.put(row.getCell(0).getStringCellValue(), data);
	            
	    }
		}catch(Exception e)
		{
			extentTest.log(Status.ERROR, "Reading Setup Sheet was un-successful.");
			extentTest.log(Status.ERROR,Throwables.getRootCause(e));
		}
	    
		return setupData;
	    }
	
	public static String[][] readTestData(ExtentTest extentTest,File filePath,int start,int size) throws Exception{

	    String[][] testData = null;
		try{
	    FileInputStream inputStream = new FileInputStream(filePath);

	    Workbook dataWorkbook = null;
	    
	    dataWorkbook = new XSSFWorkbook(inputStream);
	    Sheet dataSheet = dataWorkbook.getSheetAt(0);
	    int rowCount = dataSheet.getPhysicalNumberOfRows()-1;
	    int end = start+size-1;
	    if(start>rowCount)
	    {
	    	throw new Exception("Data is not present from staring Row "+start);
	    }
	    if(rowCount<end)
	    {
	    	end=rowCount;
	    	size = (end-start)+1;
	    }
	    int colCount=dataSheet.getRow(0).getLastCellNum();
	    int dataColumnSize=colCount+1;
	    testData=new String[size][dataColumnSize];
	    //Create a loop over all the rows of excel file to read it
	   for (int i = 0; i < size; i++) {

	        Row row = dataSheet.getRow(i+start);
	        for(int col=0;col<colCount;col++)
	        {
	        	testData[i][col]=row.getCell(col).getStringCellValue();
	        }   
	        testData[i][dataColumnSize-1]=String.valueOf(i+start);
	    }
		}catch(Exception e)
		{
			extentTest.log(Status.ERROR, "Reading Test Data Sheet was un-successful.");
			extentTest.log(Status.ERROR,Throwables.getRootCause(e));
		}
	    
		return testData;
	    }
	
	public static LinkedList<String> readSubjects(ExtentTest extentTest,File filePath) throws Exception{
		LinkedList<String> subjectList = new LinkedList<String>();
	    try{
	    FileInputStream inputStream = new FileInputStream(filePath);

	    Workbook dataWorkbook = null;

	    dataWorkbook = new XSSFWorkbook(inputStream);
	    Sheet dataSheet = dataWorkbook.getSheetAt(0);
	    int rowCount = dataSheet.getPhysicalNumberOfRows()-1;
	   for (int i = 0; i < rowCount; i++) {

	        Row row = dataSheet.getRow(i+1);
	        subjectList.add(row.getCell(0).getStringCellValue());
	    }
		}catch(Exception e)
		{
			extentTest.log(Status.ERROR, "Reading Subject Sheet was un-successful.");
			extentTest.log(Status.ERROR,Throwables.getRootCause(e));
		}
	    
		return subjectList;
	    }
	
	
	public static int getLastRowNumberToBeExecuted(ExtentTest extentTest,File filePath,int start,int size) throws Exception{

	    try{
	    FileInputStream inputStream = new FileInputStream(filePath);

	    Workbook dataWorkbook = null;
	    
	    dataWorkbook = new XSSFWorkbook(inputStream);
	    Sheet dataSheet = dataWorkbook.getSheetAt(0);
	    int rowCount = dataSheet.getPhysicalNumberOfRows()-1;
	    int end = start+size-1;
	    if(rowCount<end)
	    {
	    	end=rowCount;
	    	size = end-start+1;
	    }
		return end;
		}catch(Exception e)
		{
			extentTest.log(Status.ERROR, "Reading Test Data Sheet was un-successful.");
			extentTest.log(Status.ERROR,Throwables.getRootCause(e));
			return 0;
		}
	    }

}
