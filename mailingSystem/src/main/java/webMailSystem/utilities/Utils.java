package webMailSystem.utilities;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Properties;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import webMailSystem.common.DriverFactory;

public class Utils {

	/**
	 * This method will capture the screenshot
	 * @param methodName
	 * @return
	 * @throws Exception
	 */
		public static String takeScreenShot(ExtentTest extentTest,String testNameMethodName) throws Exception {
			String ScreenshotFileName;
			File scrFile = ((TakesScreenshot) DriverFactory.getInstance().getDriver()).getScreenshotAs(OutputType.FILE);
				String filePath=createDateHourDirectory(extentTest,getCurrentWorkingDirectory()+"//Screenshots");
				String fileName = testNameMethodName+"_"+new SimpleDateFormat("hh.mm.ss.SSS").format(new Date());
				ScreenshotFileName = filePath + "//"+fileName + ".png";
				FileUtils.copyFile(scrFile, new File(ScreenshotFileName));
				return ScreenshotFileName;
		}
		
		/**
		 * This method will return the Current working directory
		 * @return
		 */
		public static String getCurrentWorkingDirectory()
		{
			return System.getProperty("user.dir");
		}
		/**
		 * This method will create directory based on date and hour
		 * @param baseDirectory
		 * @return
		 * @throws Exception 
		 */
		public static String createDateHourDirectory(ExtentTest extentTest,String baseDirectory) throws Exception {
			String dirPath;
			try{
	        		String format = "yyyy-MM-dd";
	        		Date dt = new Date();
	                DateFormat dateFormatter = new SimpleDateFormat(format);
	                String date = dateFormatter.format(dt);
	                Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
	                calendar.setTime(dt);   // assigns calendar to given date 
	                int hour = calendar.get(Calendar.HOUR_OF_DAY);
	                dirPath=baseDirectory.concat("//").concat(date).concat("//").concat(String.valueOf(hour));
	                File directory = new File(dirPath);
	                if (!directory.exists()){
	                    directory.mkdirs();
	                }
			}catch(Exception e)
			{
				extentTest.log(Status.ERROR, "Unable to create directory because following exception was thrown - ");
				throw e;
			}
			return dirPath;
	    }
		public static String createDateDirectory(String baseDirectory) throws Exception {
			String dirPath;
			try{
	        		String format = "yyyy-MM-dd";
	        		Date dt = new Date();
	                DateFormat dateFormatter = new SimpleDateFormat(format);
	                String date = dateFormatter.format(dt);
	                dirPath=baseDirectory.concat("\\").concat(date);
	                File directory = new File(dirPath);
	                if (!directory.exists()){
	                    boolean isCreated=directory.mkdirs();
	                    if(!isCreated)
	                    {
	                    	throw new Exception("Unable to create the directory "+dirPath);
	                    }
	                    	
	                }
			}catch(Exception e)
			{
				throw e;
			}
			return dirPath;
	    }
		public static String createHTMLReport(Properties prop) throws Exception {
			String filePath=createDateDirectory(getCurrentWorkingDirectory()+"\\ExecutionReports\\"+prop.getProperty("ReportFolder"));
				String fileName = "AutomationReport_"+new SimpleDateFormat("hh_mm_ss_SSS").format(new Date());
				String reportPath = filePath + "\\"+fileName + ".html";
				System.out.println(String.format("Execution reports willl be found at '%s'", reportPath));
				return reportPath;
		}
		public static String createXMLReport(Properties prop) throws Exception {
			String filePath=createDateDirectory(getCurrentWorkingDirectory()+"\\XMLReports\\"+prop.getProperty("ReportFolder"));
				String fileName = "XMLReport_"+prop.getProperty("LastExecutedTime");
				String reportPath = filePath + "\\"+fileName + ".xml";
				System.out.println(String.format("Execution XML reports willl be found at '%s'", reportPath));
				return reportPath;
		}
		
		public static void setProperty(ExtentTest extentTest,Properties prop,String fileName,String propName,String value) throws Exception
		{
			OutputStream output = null;
			try{
				String folder="\\resources\\"+prop.get("ReportFolder").toString().toLowerCase();
			output = new FileOutputStream(Utils.getCurrentWorkingDirectory()+folder+"\\"+fileName);

				// set the properties value
				prop.setProperty(propName, value);

				// save properties to project root folder
				prop.store(output, null);
			}catch(Exception e)
			{
				extentTest.log(Status.FAIL, "Unable to set the property in the file '"+fileName+"' beacuse exception was thrown.");
				throw e;
			}
				finally {
				if (output != null) {
					output.close();
					
				}
				
		}
		}
		
		public static void copyFile(String from, String to) throws IOException
		{ 
			Path src = Paths.get(from); 
			Path dest = Paths.get(to); 
			if(Files.exists(src))
			{
				Files.copy(src, dest); 
				System.out.println("testng-results.xml is copied...");
			}else{
				System.out.println("testng-results.xml doesnot exists...");
			}
		}

}
