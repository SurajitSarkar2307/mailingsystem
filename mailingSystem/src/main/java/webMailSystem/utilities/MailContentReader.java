package webMailSystem.utilities;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class MailContentReader {
	
	public static String readMailContent(File fileName) throws Exception {
	    BufferedReader br = new BufferedReader(new FileReader(fileName));
	    try {
	        StringBuilder sb = new StringBuilder();
	        String line = br.readLine();

	        while (line != null) {
	            sb.append(line);
	            sb.append("\n");
	            line = br.readLine();
	        }
	        return sb.toString();
	    } catch(Exception e){
	    	System.out.println("Unable to read file...");
	    	throw e;
	    	
	    }finally{
	        br.close();
	    }
	}

}
