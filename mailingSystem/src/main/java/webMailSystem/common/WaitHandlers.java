package webMailSystem.common;

import java.util.Date;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

public class WaitHandlers {
	/**
	 * This method will wait for element to be present in the DOM
	 * @param element
	 * @param duration
	 * @param elementName
	 * @param page
	 * @throws Exception 
	 */
	public static void waitForElementToBePresent(ExtentTest extentTest,By element, int duration,String elementName , String page) throws Exception
	{
		try{
			WebDriverWait wait = new WebDriverWait(DriverFactory.getInstance().getDriver(),duration);
			wait.until(ExpectedConditions.presenceOfElementLocated(element));
		}catch(Exception e)
		{
			extentTest.log(Status.ERROR, String.format("Unable to find '%s' element on page '%s' after waiting for %d seconds.",elementName,page,duration));
			throw e;
		}
	}
	/**
	 * This method will wait for the element to be clickable
	 * @param element
	 * @param duration
	 * @param elementName
	 * @param page
	 * @throws Exception
	 */
	public static void waitForElementToBeClickable(ExtentTest extentTest,By element,int duration, String elementName , String page) throws Exception
	{
		try{
			WebDriverWait wait = new WebDriverWait(DriverFactory.getInstance().getDriver(),duration);
			wait.until(ExpectedConditions.elementToBeClickable(element));
		}catch(Exception e)
		{
			extentTest.log(Status.ERROR, String.format("'%s' element on page '%s' was not clickable after waiting for %d seconds.",elementName,page,duration));
			throw e;
		}
	}
	
	public static void waitForElementToBeVisible(ExtentTest extentTest,By element, int duration, String elementName , String page) throws Exception
	{
		try{
			WebDriverWait wait = new WebDriverWait(DriverFactory.getInstance().getDriver(),duration);
			wait.until(ExpectedConditions.visibilityOf(DriverFactory.getInstance().getDriver().findElement(element)));
		}catch(Exception e)
		{
			extentTest.log(Status.ERROR,String.format("'%s' element on page '%s' was not vissible after waiting for %d seconds.",elementName,page,duration));
			throw e;
		}
	}
	/**
	 * This method will wait for the element to be displayed
	 * @param element
	 * @param duration
	 * @param elementName
	 * @param page
	 * @throws Exception
	 */
	public static void waitForElementToBeDisplayed(ExtentTest extentTest,By element, int duration,String elementName , String page) throws Exception
	{
		boolean flag = false;
		long startTime = new Date().getTime();
		String expMsg="";
		while((!flag) && ((new Date().getTime()-startTime)/1000 < duration))
		{
			try{
				flag=DriverFactory.getInstance().getDriver().findElement(element).isDisplayed();
			}catch(Exception e)
			{
				flag=false;
				expMsg = e.getMessage();
			}
		}
		if(!flag)
		{
			if(!expMsg.equals(""))
			{
				extentTest.log(Status.ERROR,String.format("'%s' element is not displayed on '%s' page after waiting for %d seconds.",elementName,page,duration));
				throw new Exception(expMsg);
			}
			else{
				extentTest.log(Status.ERROR,String.format("'%s' element is not displayed on '%s' page after waiting for %d seconds.",elementName,page,duration));
				throw new TimeoutException();
			}
		}
	}
	/**
	 * This method will wait for the element to be enabled.
	 * @param element
	 * @param duration
	 * @param elementName
	 * @param page
	 * @throws Exception
	 */
	public static void waitForElementToBeEnabled(ExtentTest extentTest,By element, int duration,String elementName , String page) throws Exception
	{
		boolean flag = false;
		long startTime = new Date().getTime();
		String expMsg="";
		while((!flag) && ((new Date().getTime()-startTime)/1000 < duration))
		{
			try{
				flag=DriverFactory.getInstance().getDriver().findElement(element).isEnabled();
			}catch(Exception e)
			{
				flag=false;
				expMsg = e.getMessage();
			}
		}
		if(!flag)
		{
			if(!expMsg.equals(""))
			{
				extentTest.log(Status.ERROR,String.format("'%s' element is not enabled on '%s' page after waiting for %d seconds.",elementName,page,duration));
				throw new Exception();
			}
			else{
				extentTest.log(Status.ERROR,String.format("'%s' element is not enabled on '%s' page after waiting for %d seconds.",elementName,page,duration));
				throw new TimeoutException();
			}
		}
	}
	/**
	 * This method will for the specified element to disappear
	 * @param element
	 * @param duration
	 * @param elementName
	 * @param page
	 * @throws Exception
	 */
	public static void waitForElementToBeDisappear(ExtentTest extentTest,By element, int duration,String elementName , String page) throws Exception
	{
		boolean flag = true;
		long startTime = new Date().getTime();
		String expMsg="";
		while((flag) && ((new Date().getTime()-startTime)/1000 < duration))
		{
			try{
				flag=DriverFactory.getInstance().getDriver().findElement(element).isDisplayed();
			}catch(Exception e)
			{
				flag=false;
				expMsg = e.getMessage();
			}
		}
		if(flag)
		{
			if(!expMsg.equals(""))
			{
				extentTest.log(Status.ERROR,String.format("'%s' element is displayed on '%s' page after waiting for %d seconds.",elementName,page,duration));
				throw new Exception();
			}
			else{
				extentTest.log(Status.ERROR,String.format("'%s' element is displayed on '%s' page after waiting for %d seconds.",elementName,page,duration));
				throw new TimeoutException();
			}
		}
	}
	/**
	 * This method will wait for sepcifed seconds.
	 * @param seconds
	 */
	public static void WaitForTimeToElapse(int seconds)
	{
		try {
			Thread.sleep(seconds*1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
