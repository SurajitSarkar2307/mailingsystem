package webMailSystem.common;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

public abstract class WebMailBase {
	
	public static int defaultWait=30;
	
	/**
	 * This method will enter the specified text on the specified element
	 * @param element
	 * @param text
	 * @param duration
	 * @param elementName
	 * @param page
	 * @throws Exception
	 */
	public void enterText(ExtentTest extentTest,By element, String text, int duration,String elementName, String page) throws Exception
	{
		try{
		WaitHandlers.waitForElementToBeDisplayed(extentTest,element, duration, elementName, page);
		WaitHandlers.waitForElementToBeEnabled(extentTest,element, duration, elementName, page);
		DriverFactory.getInstance().getDriver().findElement(element).clear();
		DriverFactory.getInstance().getDriver().findElement(element).sendKeys(text);
		extentTest.log(Status.INFO, String.format("Successfully entered '%s' text on '%s' element on '%s' page.", text,elementName,page));
		}catch(Exception e)
		{
			extentTest.log(Status.ERROR, String.format("Unable to enter text on '%s' element on '%s' page.", elementName,page));
			throw e;
		}
	}
	/**
	 * This method will click on the specified element
	 * @param element
	  @param duration
	 * @param elementName
	 * @param page
	 * @throws Exception
	 */
	public void click(ExtentTest extentTest,By element,int duration,String elementName, String page) throws Exception
	{
		try{
		WaitHandlers.waitForElementToBeDisplayed(extentTest,element, duration, elementName, page);
		WaitHandlers.waitForElementToBeClickable(extentTest,element, duration, elementName, page);
		DriverFactory.getInstance().getDriver().findElement(element).click();
		extentTest.log(Status.INFO,String.format("Successfully clicked on '%s' element on '%s' page.", elementName,page));
		}catch(Exception e)
		{
			extentTest.log(Status.ERROR,String.format("Unable to click on '%s' element on '%s' page.", elementName,page));
			throw e;
		}
	}
	/**
	 * This method will switch to the specified frame
	 * @param frameLocator
	 * @param duration
	 * @param frameName
	 * @param page
	 * @throws Exception
	 */
	public void switchToFrame(ExtentTest extentTest,By frameLocator,int duration,String frameName,String page) throws Exception
	{
		try{
			WebDriverWait wait = new WebDriverWait(DriverFactory.getInstance().getDriver(),duration);
			wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frameLocator));
			extentTest.log(Status.INFO,String.format("Successfully switched on '%s' frame on '%s' page.", frameName,page));
		}catch(Exception e)
		{
			extentTest.log(Status.ERROR,String.format("Unable to find '%s' on page '%s' after waiting for %d seconds.",frameName,page,duration));
			throw e;
		}
	}
	/**
	 * This method will select the option form dropdown based on value.
	 * @param element
	 * @param value
	 * @param duration
	 * @param elementName
	 * @param page
	 * @throws Exception
	 */
	public void selectFromDropDownByVissibleText(ExtentTest extentTest,By element,String text,int duration,String elementName, String page) throws Exception
	{
		try{
			WaitHandlers.waitForElementToBeDisplayed(extentTest,element, duration, elementName, page);
			WaitHandlers.waitForElementToBeClickable(extentTest,element, duration, elementName, page);
			Select select = new Select(DriverFactory.getInstance().getDriver().findElement(element));
			select.selectByVisibleText(text);
			extentTest.log(Status.INFO,String.format("Successfully selected '%s' text from '%s' element on '%s' page.", text,elementName,page));
		}catch(Exception e)
			{
				extentTest.log(Status.ERROR,String.format("Unable to select '%s' text from '%s' element on '%s' page.", text,elementName,page));
				throw e;
			}
	}
}
