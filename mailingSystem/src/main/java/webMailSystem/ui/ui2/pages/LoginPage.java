package webMailSystem.ui.ui2.pages;

import org.openqa.selenium.By;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import webMailSystem.common.WaitHandlers;
import webMailSystem.common.WebMailBase;

public class LoginPage extends  WebMailBase{
	
	public LoginPage(ExtentTest extentTest) throws Exception {
		WaitHandlers.waitForElementToBeDisplayed(extentTest,submitBtn, 15, "Login Button", "Login");
	}
	/**
	 * Page Objects for Login Page
	 */
	 private By userName = By.id("rcmloginuser");
	 private By password = By.id("rcmloginpwd");
	 private By submitBtn = By.id("rcmloginsubmit");
	 
	 /**
	  * This method will enter the user name
	  * @param uName
	  * @return
	  * @throws Exception
	  */
	 private LoginPage enterUserName(ExtentTest extentTest,String uName) throws Exception
	 {
		 enterText(extentTest,userName, uName, defaultWait, "User Name", "Login");
		 return this;
	 }
	 /**
	  * This method will enter the password
	  * @param uName
	  * @return
	  * @throws Exception
	  */
	 private LoginPage enterPassword(ExtentTest extentTest,String pass) throws Exception
	 {
		 enterText(extentTest,password, pass, defaultWait, "Password", "Login");
		 return this;
	 }
	 /**
	  * This method will enter the password
	  * @param uName
	  * @return
	  * @throws Exception
	  */
	 private WelcomePage clickOnSubmitBtn(ExtentTest extentTest) throws Exception
	 {
		 click(extentTest,submitBtn, defaultWait, "Submit", "Login");
		 return new WelcomePage(extentTest);
	 }
	 /**
	  * This method will perform the login
	  * @param uName
	  * @param pass
	  * @return
	  * @throws Exception
	  */
	 public WelcomePage loginToWebmail(ExtentTest extentTest,String uName,String pass) throws Exception
	 {
		 
		 WelcomePage welcomePage= enterUserName(extentTest,uName).enterPassword(extentTest,pass).clickOnSubmitBtn(extentTest);
		 if(welcomePage!=null)
		 {
			 extentTest.log(Status.INFO, String.format("Login Successful using credentials - '%s' , '%s'",uName,pass));
		 }
		 else{
			 extentTest.log(Status.ERROR, String.format("Login was not Successful using credentials - '%s' , '%s'",uName,pass));
		 }
		 return welcomePage;
	 }
	 
	 
	 
	 
}
