package webMailSystem.ui.ui2.pages;

import org.openqa.selenium.By;

import com.aventstack.extentreports.ExtentTest;

import webMailSystem.common.DriverFactory;
import webMailSystem.common.WaitHandlers;
import webMailSystem.common.WebMailBase;

public class HeaderPage extends WebMailBase{
	public HeaderPage(ExtentTest extentTest) throws Exception
	{
		WaitHandlers.waitForElementToBeDisplayed(extentTest,logOutBtn, defaultWait, "Logout Btn", "Header");
	}
	
	public By logOutBtn = By.id("rcmbtn101");
	
	public LoginPage clickOnLogout(ExtentTest extentTest) throws Exception
	{
		 click(extentTest,logOutBtn, defaultWait, "Logout", "Header");
		 try{
			 DriverFactory.getInstance().getDriver().switchTo().alert().accept();
		 }catch(Exception e)
		 {
			 
		 }
		 return new LoginPage(extentTest);
	}

}
