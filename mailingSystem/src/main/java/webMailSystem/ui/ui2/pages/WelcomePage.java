package webMailSystem.ui.ui2.pages;

import org.openqa.selenium.By;

import com.aventstack.extentreports.ExtentTest;
import webMailSystem.common.WaitHandlers;
import webMailSystem.common.WebMailBase;

public class WelcomePage extends WebMailBase{
	
	public WelcomePage(ExtentTest extentTest) throws Exception
	{
		try{
		//switchToFrame(extentTest,mailFrame, defaultWait, "Mail Frame", "Welcome");
		WaitHandlers.waitForElementToBeDisappear(extentTest,loadingTile, defaultWait, "Loading Tile", "Welcome");
		WaitHandlers.waitForElementToBeDisplayed(extentTest,composeMailBtn, 15, "Compose Mail Button", "Welcome");
		//DriverFactory.getInstance().getDriver().switchTo().defaultContent();
		}catch(Exception e)
		{
			//WaitHandlers.WaitForTimeToElapse(2);
			//switchToFrame(extentTest,mailFrame, defaultWait, "Mail Frame", "Welcome");
			//WaitHandlers.waitForElementToBeDisappear(extentTest,loadingTile, defaultWait, "Loading Tile", "Welcome");
			//WaitHandlers.waitForElementToBeDisplayed(extentTest,composeMailBtn, 15, "Compose Mail Button", "Welcome");
			//DriverFactory.getInstance().getDriver().switchTo().defaultContent();
			throw e;
		}
	}
	//private By mailFrame = By.id("mailFrame");
	//private By mailTab = By.id("rcmbtn102");
	private By composeMailBtn = By.xpath("//a[text()='Compose']");
	private By loadingTile = By.xpath("/div[@class='loading']");
	/**
	 * This method will click on the Compose mail button
	 * @return
	 * @throws Exception 
	 */
	public ComposeMailPage clickOnComposeMailBtn(ExtentTest extentTest) throws Exception
	{
		ComposeMailPage composeMailPage;
		WaitHandlers.waitForElementToBeDisappear(extentTest,loadingTile, defaultWait, "Loading Tile", "Welcome");
		click(extentTest,composeMailBtn, defaultWait, "Compose Mail Button", "Welcome");
		WaitHandlers.waitForElementToBeDisappear(extentTest, composeMailBtn, 15, "Compose Mail Button", "Welcome");
		composeMailPage = new ComposeMailPage(extentTest);
		return composeMailPage;
	}
}
