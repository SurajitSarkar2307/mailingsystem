package webMailSystem.ui.u1.pages;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.UnhandledAlertException;
import com.aventstack.extentreports.ExtentTest;
import webMailSystem.common.DriverFactory;
import webMailSystem.common.WaitHandlers;
import webMailSystem.common.WebMailBase;

public class ComposeMailPage extends WebMailBase{
	
	public ComposeMailPage(ExtentTest extentTest) throws Exception
	{
		WaitHandlers.waitForElementToBeDisappear(extentTest,loadingTile, defaultWait, "Loading Tile", "Compose Mail");
		WaitHandlers.waitForElementToBeDisplayed(extentTest,subject, defaultWait, "Subject", "Compose Mail");
	}
	private By toList = By.id("_to");
	public By subject = By.id("compose-subject");
	private By sendBtn = By.xpath("//a[text()='Send']");
	private By loadingTile = By.xpath("//div[@id='messagestack']/div[@class='loading']");
	private By editorTypeDropdown = By.xpath("//select[@name='editorSelector']");
	private By insertSourceCodeBtn = By.xpath("//div[@aria-label='Source code']/button/i");
	private By souceCodeTextAea = By.xpath("//div[@class='mce-title'][text()='Source code']/../..//textarea");
	private By sourceCodeOkBtn = By.xpath("//button[@role='presentation']/span[contains(text(),'Ok')]");
	
	/**
	  * This method will enter the To email id
	  * @param email
	  * @return
	  * @throws Exception
	  */
	 private ComposeMailPage enterTo(ExtentTest extentTest,String email) throws Exception
	 {
		 enterText(extentTest,toList, email, defaultWait, "To List", "Compose Mail");
		 return this;
	 }
	 
	 /**
	  * This method will enter the subject
	  * @param subject
	  * @return
	  * @throws Exception
	  */
	 private ComposeMailPage enterSubject(ExtentTest extentTest,String sub) throws Exception
	 {
		 enterText(extentTest,subject, sub, defaultWait, "Subject", "Compose Mail");
		 return this;
	 }
	 /**
	  * This method will click on the Send button
	  * @return
	  * @throws Exception
	  */
	 /*private WelcomePage clickOnSendMailBtn(ExtentTest extentTest) throws Exception
	 {
		 click(extentTest,sendBtn, defaultWait, "Send Button", "Compose Mail");
		 DriverFactory.getInstance().getDriver().switchTo().defaultContent();
		 return new WelcomePage(extentTest);
	 }*/
	/**
	 * This method will compose a plain text mail and send
	 * @param to
	 * @param sub
	 * @param mailBody
	 * @return
	 * @throws Exception
	 */
	/* public WelcomePage composePlainTextMail(ExtentTest extentTest,String to,String sub,String mailBody) throws Exception
	 {
		 return enterTo(extentTest,to).enterSubject(extentTest,sub).enterPlainTextMailBody(extentTest,mailBody).clickOnSendMailBtn(extentTest);
	 }*/
	/**
	 * This method will select the editorType
	 * @param value
	 * @return
	 * @throws Exception
	 */
	 private ComposeMailPage selectEditorTypeByVissibleText(ExtentTest extentTest,String value) throws Exception
	 {
		 selectFromDropDownByVissibleText(extentTest,editorTypeDropdown, value, defaultWait, "Editor type dropdown", "Compose Mail");
		 WaitHandlers.waitForElementToBeClickable(extentTest,insertSourceCodeBtn, defaultWait, "Insert Source Code Button", "Compose Mail");
		 return this;
	 }
	 
	 private ComposeMailPage clickOnInsertSourceCodeBtn(ExtentTest extentTest) throws Exception
	 {
		 click(extentTest,insertSourceCodeBtn, defaultWait, "Insert Source Code Button", "Compose Mail");
		 WaitHandlers.waitForElementToBeEnabled(extentTest,souceCodeTextAea, defaultWait, "Source Code Text Area", "Compose Mail - Source code");
		 return this;
	 }
	 private ComposeMailPage enterSourceCode(ExtentTest extentTest,String sourceCode) throws Exception
	 {
		 enterText(extentTest,souceCodeTextAea, sourceCode, defaultWait, "Source Code Text Area", "Compose Mail - Source code");
		 return this;
	 }
	 private ComposeMailPage clickOnSourceCodeOKBtn(ExtentTest extentTest) throws Exception
	 {
		 click(extentTest,sourceCodeOkBtn, defaultWait, "Source Code OK Button", "Compose Mail");
		 return this;
	 }
	 /**
	  * This method will fetch the value from the To email id field
	  * @return
	  * @throws Exception
	  */
	 private String getToFieldData(ExtentTest extentTest) throws Exception
	 {
		return DriverFactory.getInstance().getDriver().findElement(toList).getAttribute("value");
		
	 }
	 /**
		 * This method will compose a plain text mail and send
		 * @param to
		 * @param sub
		 * @param mailBody
		 * @return
		 * @throws Exception
		 */
		 public WelcomePage composeHTMLMail(ExtentTest extentTest,String to,String sub,String sourceCode) throws Exception
		 {
			 enterTo(extentTest,to).enterSubject(extentTest,sub).selectEditorTypeByVissibleText(extentTest,"HTML").clickOnInsertSourceCodeBtn(extentTest)
					 .enterSourceCode(extentTest,sourceCode).clickOnSourceCodeOKBtn(extentTest);
			 String toList = getToFieldData(extentTest);
			 if(toList.equals(""))
			 {
				 enterTo(extentTest,to);
			 }
			 
			 try{
				 click(extentTest,sendBtn, defaultWait, "Send Button", "Compose Mail");
				 
				 WaitHandlers.waitForElementToBeDisappear(extentTest,loadingTile, defaultWait, "Loading Tile", "Welcome");
				 WaitHandlers.waitForElementToBeDisappear(extentTest,sendBtn, 15, "Send Button", "Compose Mail");
				 DriverFactory.getInstance().getDriver().switchTo().defaultContent();
			 }catch (UnhandledAlertException e) {
					Alert alert= DriverFactory.getInstance().getDriver().switchTo().alert();
					alert.accept();
					enterTo(extentTest,to).enterSubject(extentTest,sub).selectEditorTypeByVissibleText(extentTest,"HTML").clickOnInsertSourceCodeBtn(extentTest)
					 .enterSourceCode(extentTest,sourceCode).clickOnSourceCodeOKBtn(extentTest);
					click(extentTest,sendBtn, defaultWait, "Send Button", "Compose Mail");
					WaitHandlers.waitForElementToBeDisappear(extentTest,loadingTile, defaultWait, "Loading Tile", "Welcome");
					 WaitHandlers.waitForElementToBeDisappear(extentTest,sendBtn, 15, "Send Button", "Compose Mail");
					 DriverFactory.getInstance().getDriver().switchTo().defaultContent();
			 }
			 return new WelcomePage(extentTest);
			 
		 }
	 

}
